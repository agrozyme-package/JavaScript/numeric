export { Numeric, toBigInt, toBigInteger, toBigNumber, toBN, toIntegerString, toNumeric, toNumerics } from './numeric';
export type { NumericValue } from './numeric';

export { formatValue } from './format';
export type { IFormatNumberOptions } from './format';

export { getNumericConfig, setupNumericConfig } from './config';
export { MaxInt256, MaxUint256, MinInt256, NegativeOne, One, Two, WeiPerEther, Zero } from './constants';

export {
  createArrayCompareFunction,
  isInteger,
  isRange,
  isSignedIntegerBits,
  isUnsignedIntegerBits,
  tryParse,
} from './helper';
export type { RangeOptions } from './helper';

/**
 * - must use `@types/node@16` for Symbol typing
 * - must put `@types/bn.js` in dependencies section of package.json
 * - must set `rootDir` to `src` that in compilerOptions section of tscnfig.json (for bundler tools)
 * - must import namespaces: `big-integer`, `bn.js`, `decimal.js`
 * */
