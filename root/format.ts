import formatter from 'format-number';
import { Numeric, NumericValue } from './numeric';

export interface IFormatNumberOptions {
  negativeType?: 'right' | 'left' | 'brackets' | 'none';
  negativeLeftSymbol?: string;
  negativeRightSymbol?: string;
  negativeLeftOut?: boolean;
  negativeRightOut?: boolean;
  prefix?: string;
  suffix?: string;
  integerSeparator?: string;
  decimalsSeparator?: string;
  decimal?: string;
  padLeft?: number;
  padRight?: number;
  round?: number;
  truncate?: number;
}

export const formatValue = (value: NumericValue, options?: IFormatNumberOptions) => {
  const format = formatter(options);
  const text = new Numeric(value).toFixed();
  return format(<any>text);
};
