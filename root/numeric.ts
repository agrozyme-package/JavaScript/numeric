import { BigNumber, BigNumberish } from '@ethersproject/bignumber';
import { hexlify, isBytesLike } from '@ethersproject/bytes';
import 'big-integer';
import bigInt from 'big-integer';
import 'bn.js';
import BN from 'bn.js';
import { isNode } from 'browser-or-node';
import 'decimal.js';
import { Decimal } from 'decimal.js';
import { InspectOptionsStylized } from 'util';

export type NumericValue = Numeric | Decimal.Value | bigInt.BigNumber | BigNumberish;

export const toNumeric = (value: NumericValue) => new Numeric(value);
export const toNumerics = (values: NumericValue[]) => values.map(toNumeric);
export const toIntegerString = (value: NumericValue) => toNumeric(value).toFixed(0);
export const toBigInt = (value: NumericValue) => BigInt(toIntegerString(value));
export const toBN = (value: NumericValue) => new BN(toIntegerString(value));
export const toBigInteger = (value: NumericValue) => bigInt(toIntegerString(value));
export const toBigNumber = (value: NumericValue) => BigNumber.from(toIntegerString(value));

// noinspection JSUnusedGlobalSymbols
export class Numeric extends Decimal {
  // private readonly toStringTag: string = 'Decimal';

  constructor(value: NumericValue) {
    if (value instanceof Decimal) {
      super(value);
    } else {
      switch (typeof value) {
        case 'string':
        case 'number':
          super(value);
          break;
        case 'bigint':
          super(value.toString());
          break;
        default:
          if (bigInt.isInstance(value) || BigNumber.isBigNumber(value) || BN.isBN(value)) {
            super(value.toString());
          } else if (isBytesLike(value)) {
            super(hexlify(value));
          } else {
            super(value);
          }
          break;
      }
    }
  }

  // get [Symbol.toStringTag]() {
  //   return 'Numeric';
  // }

  static abs(value: NumericValue) {
    return new Numeric(value).abs();
  }

  static acos(value: NumericValue) {
    return new Numeric(value).acos();
  }

  static acosh(value: NumericValue) {
    return new Numeric(value).acosh();
  }

  static add(x: NumericValue, y: NumericValue) {
    return new Numeric(x).add(y);
  }

  static asin(value: NumericValue) {
    return new Numeric(value).asin();
  }

  static asinh(value: NumericValue) {
    return new Numeric(value).asinh();
  }

  static atan(value: NumericValue) {
    return new Numeric(value).atan();
  }

  static atanh(value: NumericValue) {
    return new Numeric(value).atanh();
  }

  static atan2(x: NumericValue, y: NumericValue) {
    return new Numeric(Decimal.atan2(new Numeric(x), new Numeric(y)));
  }

  static cbrt(value: NumericValue) {
    return new Numeric(value).cbrt();
  }

  static ceil(value: NumericValue) {
    return new Numeric(value).ceil();
  }

  static clamp(value: NumericValue, min: NumericValue, max: NumericValue) {
    return new Numeric(Decimal.clamp(new Numeric(value), new Numeric(min), new Numeric(max)));
  }

  static cos(value: NumericValue) {
    return new Numeric(value).cos();
  }

  static cosh(value: NumericValue) {
    return new Numeric(value).cosh();
  }

  static div(x: NumericValue, y: NumericValue) {
    return new Numeric(x).div(y);
  }

  static exp(value: NumericValue) {
    return new Numeric(value).exp();
  }

  static floor(value: NumericValue) {
    return new Numeric(value).floor();
  }

  static hypot(...values: NumericValue[]) {
    return new Numeric(Decimal.hypot(...toNumerics(values)));
  }

  static ln(value: NumericValue) {
    return new Numeric(value).ln();
  }

  static log(value: NumericValue, base?: NumericValue) {
    return new Numeric(value).log(base);
  }

  static log2(value: NumericValue) {
    return new Numeric(value).log(2);
  }

  static log10(value: NumericValue) {
    return new Numeric(value).log(10);
  }

  static max(...values: NumericValue[]) {
    return new Numeric(Decimal.max(...toNumerics(values)));
  }

  static min(...values: NumericValue[]) {
    return new Numeric(Decimal.min(...toNumerics(values)));
  }

  static mod(x: NumericValue, y: NumericValue) {
    return new Numeric(x).mod(y);
  }

  static mul(x: NumericValue, y: NumericValue) {
    return new Numeric(x).mul(y);
  }

  static pow(base: NumericValue, exponent: NumericValue) {
    return new Numeric(base).pow(exponent);
  }

  static random(significantDigits?: number) {
    return new Numeric(Decimal.random(significantDigits));
  }

  static round(value: NumericValue) {
    return new Numeric(value).round();
  }

  static set(object: Decimal.Config) {
    return Decimal.set(object);
  }

  static sign(value: NumericValue) {
    return Decimal.sign(new Numeric(value));
  }

  static sin(value: NumericValue) {
    return new Numeric(value).sin();
  }

  static sinh(value: NumericValue) {
    return new Numeric(value).sinh();
  }

  static sqrt(value: NumericValue) {
    return new Numeric(value).sqrt();
  }

  static sub(x: NumericValue, y: NumericValue) {
    return new Numeric(x).sub(y);
  }

  static sum(...values: NumericValue[]) {
    return new Numeric(Decimal.sum(...toNumerics(values)));
  }

  static tan(value: NumericValue) {
    return new Numeric(value).tan();
  }

  static tanh(value: NumericValue) {
    return new Numeric(value).tanh();
  }

  static trunc(value: NumericValue) {
    return new Numeric(value).trunc();
  }

  absoluteValue() {
    return new Numeric(super.absoluteValue());
  }

  abs() {
    return new Numeric(super.abs());
  }

  ceil() {
    return new Numeric(super.ceil());
  }

  clampedTo(min: NumericValue, max: NumericValue) {
    return new Numeric(super.clampedTo(new Numeric(min), new Numeric(max)));
  }

  clamp(min: NumericValue, max: NumericValue) {
    return new Numeric(super.clamp(new Numeric(min), new Numeric(max)));
  }

  comparedTo(value: NumericValue) {
    return super.comparedTo(new Numeric(value));
  }

  cmp(value: NumericValue) {
    return super.cmp(new Numeric(value));
  }

  cosine() {
    return new Numeric(super.cosine());
  }

  cos() {
    return new Numeric(super.cos());
  }

  cubeRoot() {
    return new Numeric(super.cubeRoot());
  }

  cbrt() {
    return new Numeric(super.cbrt());
  }

  dividedBy(value: NumericValue) {
    return new Numeric(super.dividedBy(new Numeric(value)));
  }

  div(value: NumericValue) {
    return new Numeric(super.div(new Numeric(value)));
  }

  dividedToIntegerBy(value: NumericValue) {
    return new Numeric(super.dividedToIntegerBy(new Numeric(value)));
  }

  divToInt(value: NumericValue) {
    return new Numeric(super.divToInt(new Numeric(value)));
  }

  equals(value: NumericValue) {
    return super.equals(new Numeric(value));
  }

  eq(value: NumericValue) {
    return super.eq(new Numeric(value));
  }

  floor() {
    return new Numeric(super.floor());
  }

  greaterThan(value: NumericValue) {
    return super.greaterThan(new Numeric(value));
  }

  gt(value: NumericValue) {
    return super.gt(new Numeric(value));
  }

  greaterThanOrEqualTo(value: NumericValue) {
    return super.greaterThanOrEqualTo(new Numeric(value));
  }

  gte(value: NumericValue) {
    return super.gte(new Numeric(value));
  }

  hyperbolicCosine() {
    return new Numeric(super.hyperbolicCosine());
  }

  cosh() {
    return new Numeric(super.cosh());
  }

  hyperbolicSine() {
    return new Numeric(super.hyperbolicSine());
  }

  sinh() {
    return new Numeric(super.sinh());
  }

  hyperbolicTangent() {
    return new Numeric(super.hyperbolicTangent());
  }

  tanh() {
    return new Numeric(super.tanh());
  }

  inverseCosine() {
    return new Numeric(super.inverseCosine());
  }

  acos() {
    return new Numeric(super.acos());
  }

  inverseHyperbolicCosine() {
    return new Numeric(super.inverseHyperbolicCosine());
  }

  acosh() {
    return new Numeric(super.acosh());
  }

  inverseHyperbolicSine() {
    return new Numeric(super.inverseHyperbolicSine());
  }

  asinh() {
    return new Numeric(super.asinh());
  }

  inverseHyperbolicTangent() {
    return new Numeric(super.inverseHyperbolicTangent());
  }

  atanh() {
    return new Numeric(super.atanh());
  }

  inverseSine() {
    return new Numeric(super.inverseSine());
  }

  asin() {
    return new Numeric(super.asin());
  }

  inverseTangent() {
    return new Numeric(super.inverseTangent());
  }

  atan() {
    return new Numeric(super.atan());
  }

  lessThan(value: NumericValue) {
    return super.lessThan(new Numeric(value));
  }

  lt(value: NumericValue) {
    return super.lt(new Numeric(value));
  }

  lessThanOrEqualTo(value: NumericValue) {
    return super.lessThanOrEqualTo(new Numeric(value));
  }

  lte(value: NumericValue) {
    return super.lte(new Numeric(value));
  }

  logarithm(value?: NumericValue) {
    return new Numeric(super.logarithm(undefined === value ? undefined : new Numeric(value)));
  }

  log(value?: NumericValue) {
    return new Numeric(super.log(undefined === value ? undefined : new Numeric(value)));
  }

  minus(value: NumericValue) {
    return new Numeric(super.minus(new Numeric(value)));
  }

  sub(value: NumericValue) {
    return new Numeric(super.sub(new Numeric(value)));
  }

  modulo(value: NumericValue) {
    return new Numeric(super.modulo(new Numeric(value)));
  }

  mod(value: NumericValue) {
    return new Numeric(super.mod(new Numeric(value)));
  }

  naturalExponential() {
    return new Numeric(super.naturalExponential());
  }

  exp() {
    return new Numeric(super.exp());
  }

  naturalLogarithm() {
    return new Numeric(super.naturalLogarithm());
  }

  ln() {
    return new Numeric(super.ln());
  }

  negated() {
    return new Numeric(super.negated());
  }

  neg() {
    return new Numeric(super.neg());
  }

  plus(value: NumericValue) {
    return new Numeric(super.plus(new Numeric(value)));
  }

  add(value: NumericValue) {
    return new Numeric(super.add(new Numeric(value)));
  }

  round() {
    return new Numeric(super.round());
  }

  sine() {
    return new Numeric(super.sine());
  }

  sin() {
    return new Numeric(super.sin());
  }

  squareRoot() {
    return new Numeric(super.squareRoot());
  }

  sqrt() {
    return new Numeric(super.sqrt());
  }

  tangent() {
    return new Numeric(super.tangent());
  }

  tan() {
    return new Numeric(super.tan());
  }

  times(value: NumericValue) {
    return new Numeric(super.times(new Numeric(value)));
  }

  mul(value: NumericValue) {
    return new Numeric(super.mul(new Numeric(value)));
  }

  toDecimalPlaces(decimalPlaces?: number, rounding?: Decimal.Rounding) {
    return new Numeric(
      undefined !== rounding && undefined !== decimalPlaces
        ? super.toDecimalPlaces(decimalPlaces, rounding)
        : super.toDecimalPlaces(decimalPlaces)
    );
  }

  toDP(decimalPlaces?: number, rounding?: Decimal.Rounding) {
    return new Numeric(
      undefined !== rounding && undefined !== decimalPlaces
        ? super.toDP(decimalPlaces, rounding)
        : super.toDP(decimalPlaces)
    );
  }

  toFraction(maxDenominator?: NumericValue) {
    return toNumerics(super.toFraction(undefined === maxDenominator ? undefined : new Numeric(maxDenominator)));
  }

  toNearest(value: NumericValue, rounding?: Decimal.Rounding) {
    return new Numeric(super.toNearest(new Numeric(value), rounding));
  }

  toPower(value: NumericValue) {
    return new Numeric(super.toPower(new Numeric(value)));
  }

  pow(value: NumericValue) {
    return new Numeric(super.pow(new Numeric(value)));
  }

  toSignificantDigits(decimalPlaces?: number, rounding?: Decimal.Rounding) {
    return new Numeric(
      undefined !== rounding && undefined !== decimalPlaces
        ? super.toSignificantDigits(decimalPlaces, rounding)
        : super.toSignificantDigits(decimalPlaces)
    );
  }

  toSD(decimalPlaces?: number, rounding?: Decimal.Rounding) {
    return new Numeric(
      undefined !== rounding && undefined !== decimalPlaces
        ? super.toSD(decimalPlaces, rounding)
        : super.toSD(decimalPlaces)
    );
  }

  truncated() {
    return new Numeric(super.truncated());
  }

  trunc() {
    return new Numeric(super.trunc());
  }

  inspect(depth: number, options: InspectOptionsStylized) {
    const value = this.toFixed();
    return isNode ? options.stylize(value, 'bigint') : value;
  }

  // [Symbol.for('nodejs.util.inspect.custom')](depth: number, options: InspectOptionsStylized) {
  //   return this.inspect(depth, options);
  // }
}
